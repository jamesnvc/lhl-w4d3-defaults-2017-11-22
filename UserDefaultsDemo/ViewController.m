//
//  ViewController.m
//  UserDefaultsDemo
//
//  Created by James Cash on 22-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) IBOutlet UISlider *slider;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *deNfaults = [NSUserDefaults standardUserDefaults];
    self.slider.value = [defaults floatForKey:@"sliderValue"];
    self.textField.text = [defaults objectForKey:@"textFieldContent"];
}

- (IBAction)sliderMove:(UISlider*)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:sender.value forKey:@"sliderValue"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    NSString *newText = [textField.text
                         stringByReplacingCharactersInRange:range
                         withString:string];

    [[NSUserDefaults standardUserDefaults] setObject:newText forKey:@"textFieldContent"];
    return YES;
}

@end
